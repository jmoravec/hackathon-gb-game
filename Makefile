OBJS = cb_gb.asm
OBJS_NAME = cb_gb
OBJ_NAME = cb_gb

BDIR = build

ASM = rgbasm
LINK = rgblink
FIX = rgbfix

all : $(OBJS)
	$(ASM) -o$(BDIR)/$(OBJS_NAME).obj $(OBJS)
	$(LINK) -m$(BDIR)/$(OBJ_NAME).map -n$(BDIR)/$(OBJ_NAME).sym -o$(BDIR)/$(OBJ_NAME).gb $(BDIR)/$(OBJS_NAME).obj
	$(FIX) -p 0 -v $(BDIR)/$(OBJ_NAME).gb
	rm $(BDIR)/$(OBJS_NAME).obj