include "hardware.inc"
include "header.asm"
;include "structs.inc"
include "structs.asm"
include "utils.asm"
include "vars.asm"
include "player.asm"
include "weapon.asm"
include "enemy.asm"
include "levels.asm"

section "Game Code", rom0

START:
    ;enable vblank interrupt
    ei
    ld  sp, $FFFE
    ld  a, IEF_VBLANK
    ld  [rIE], a

    call WAIT_VBLANK

    ; turn off lcd
    ld a, $0
    ldh [rLCDC], a
	ldh [rSTAT], a

    ; clear everything
    call CLEAR_MAP
    call CLEAR_OAM
    call CLEAR_RAM

    call INIT_WINDOW

    ; setup DMA transfer
    call DMA_COPY

.init_logo
    call LOAD_FONT

    call SETUP_LOGO
    ; turn lcd on
    ld a, LCDCF_ON|LCDCF_BGON|LCDCF_BG8000|LCDCF_OBJ16|LCDCF_OBJON
    ld [rLCDC], a

    call WAIT_UNTIL_START

    ; turn lcd off
    call WAIT_VBLANK
    ld a, $0
    ldh [rLCDC], a
	ldh [rSTAT], a

    call INIT_GAME_PALLETE

    call CLEAR_MAP
    call CLEAR_RAM
    call CLEAR_OAM

    ; setup sprites
    ; load Player sprite
    ld hl, _VRAM    ; ld Sprite into location $9000
    ld de, SpriteStart
    ld bc, SpriteEnd - SpriteStart
    call COPY_SPRITE

    ld hl, $9000    ; ld backgound tiles into $8800 bank
    ld de, BackgroundStart
    ld bc, BackgroundEnd - BackgroundStart
    call COPY_SPRITE

    ld hl, _SCRN0
    ld de, MapStart
    ld bc, MapEnd - MapStart
    call COPY_SPRITE

    ; Setup the Player
    call PLAYER_INIT
    call WEAPON_INIT

    ; Setup the Enemies
    ld a, 1
    ld [current_level], a
    call SETUP_LEVEL

    ; turn lcd on
    ld a, LCDCF_ON|LCDCF_BGON|LCDCF_BG8800|LCDCF_OBJ16|LCDCF_OBJON
    ld [rLCDC], a

MAIN_LOOP:
    call WAIT_VBLANK
    call READ_JOYPAD
    call UPDATE_PLAYER
    call UPDATE_WEAPON
    call UPDATE_ENEMIES
    call DRAW_PLAYER
    call DRAW_WEAPON
    call DRAW_ENEMIES
    call CHECK_IF_PLAYER_HIT
    cp 0
    jp nz, .lockup
    call CHECK_IF_LEVEL_COMPLETED
    call _HRAM              ; initiate DMA transfer

    jr MAIN_LOOP


.lockup
    halt
    jr .lockup

SETUP_LOGO:
    ; Load the logo tileset into memory
    ld hl, _VRAM    ; ld Sprite into location $9000
    ld de, LogoStart
    ld bc, LogoEnd - LogoStart
    call COPY_SPRITE

    ; Load the logo map into memory
    ld hl, _SCRN0
    ld de, LogoMapStart
    ld bc, LogoMapEnd - LogoMapStart
    call COPY_SPRITE

    ; Show the Press Start! text
    ld hl, _SCRN0 + $1E4     ; yes i got this from trial and error :|
    ld de, PressStartString
    ld bc, PressStartStringEnd
    call COPY_STRING

    ; set the pallete for the logo screen
    ld a, %00000010     ; pallete
    ld [rBGP], a
    ret

WAIT_UNTIL_START:
.loop
    call WAIT_VBLANK
    call READ_JOYPAD
    ; check if start button pressed
    ld a, [pad_state]
    and $F0
    swap a
    and P1F_3       ; start button
    jr z, .loop
    ret

LOAD_FONT:
    ld hl, $8800
    ld de, FontStart
    ld bc, FontEnd - FontStart
    call COPY_SPRITE
    ret




Section "Sprites", rom0
SpriteStart:
incbin "assets/player-2.chr"
;incbin "assets/sword.chr"
incbin "assets/keyboard-down.chr"
incbin "assets/keyboard-up.chr"
incbin "assets/keyboard-right.chr"
incbin "assets/Green Robot + Enemy Update - Original(1).chr"
SpriteEnd:
BackgroundStart:
; setup a blank tile
REPT 16
db $0
ENDR
incbin "assets/wall1.chr"
incbin "assets/ground.chr"
BackgroundEnd:
; https://opengameart.org/content/32x32-fantasy-tileset - didn't end up using (cause 32x32) but 
; background sprites are based from it
; https://manugamesdev.itch.io/real-time-combat     ; player char
; https://opengameart.org/content/green-robot-enemy ; 

Section "Map", rom0
MapStart:
db $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3 
db $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4 

REPT 14
db $1, $3, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $5, $7, $1, $3
db $2, $4, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $6, $8, $2, $4
ENDR

db $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3, $1, $3 
db $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4, $2, $4 
MapEnd:

Section "Logo Tiles", romx
LogoStart:
incbin "assets/logo-good.chr"
LogoEnd:
FontStart:
incbin "assets/font.chr"
FontEnd:

Section "Logo Map", romx
LogoMapStart:
db 0,0,0,0,0,0,0,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,3,4,5,6,7,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,9,10,11,12,13,14,15,16,17,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,19,10,20,21,22,23,24,25,26,27,28,29,30,31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,32,33,34,23,35,21,36,37,28,38,39,40,41,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,43,23,44,33,45,46,47,48,49,50,41,51,52,53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,54,55,56,57,58,0,0,59,41,60,49,61,62,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,63,57,64,55,65,0,0,66,67,68,69,70,71,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,72,73,29,74,75,71,0,76,77,78,79,80,81,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,16,83,84,85,11,86,87,88,89,90,91,92,79,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,93,94,29,95,96,97,5,98,99,100,89,29,101,102,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,103,104,105,11,106,107,108,109,16,110,111,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,103,113,114,5,115,116,117,47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,118,119,120,121,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
LogoMapEnd:

Section "Strings", romx
PressStartString:
db "Press START!", 0
PressStartStringEnd: