Installation
============

1. Install https://github.com/rednex/rgbds
1. Install Make
1. Run `mkdir -p build`
1. Run `make` in the main directory
