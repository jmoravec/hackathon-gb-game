    struct Character
    bytes 1, XPos               ; 1 byte
    bytes 1, YPos               ; 1 byte
    bytes 1, LeftTileNumber     ; 1 byte
    bytes 1, RightTileNumber    ; 1 byte
    bytes 1, Flags              ; 1 byte
    bytes 1, GameFlags          ; 000|_TH_ON|_DEFENSE_ON|_PLAYER_ATTACKING|_PLAYER_USE_ALT_SPRITE|_PLAYER_IS_WALKING
    bytes 1, WalkingTimer       ; 1 byte
    bytes 1, Direction          ; 0000|_PLAYER_UP|_PLAYER_DOWN|_PLAYER_LEFT|_PLAYER_RIGHT
    end_struct

_TH_ON equ %00010000
_DEFENSE_ON equ %00001000
_PLAYER_ATTACKING equ %00000100
_PLAYER_USE_ALT_SPRITE equ %00000010
_PLAYER_IS_WALKING equ %00000001

_PLAYER_UP equ %00001000
_PLAYER_DOWN equ %00000100
_PLAYER_LEFT equ %00000010
_PLAYER_RIGHT equ %00000001


    struct Weapon
    bytes 1, XPos
    bytes 1, YPos
    bytes 1, LeftTileNumber
    bytes 1, RightTileNumber
    bytes 1, Flags
    bytes 1, AnimateTimer
    end_struct

    struct Enemy
    bytes 1, XPos               ; absolute x/y pos on the map, not the screen, so range of 0-255
    bytes 1, YPos               
    bytes 1, LeftTileNumber
    bytes 1, RightTileNumber
    bytes 1, Attributes         ; 000000|_ENEMY_HORIZONTAL_ONLY|_ENEMY_IS_DEAD
    bytes 1, Flags
    bytes 1, ScreenXPos         ; position relative to the screen
    bytes 1, ScreenYPos
    bytes 1, CurrentDirection   ; 000000|_ENEMY_MOVING_UP|_ENEMY_MOVING_RIGHT
    end_struct

_ENEMY_IS_DEAD equ %00000001
_ENEMY_NOT_ON_SCREEN equ %00000010
_ENEMY_HORIZONTAL_ONLY equ %00000100
_ENEMY_VERTICAL_ONLY equ %00001000
_ENEMY_PLAYER_FOLLOWER equ %00010000

_ENEMY_MOVING_RIGHT equ %00000001
_ENEMY_MOVING_DOWN    equ %00000010



section "vars", wram0[$C000]
vblank_flag: db
pad_state: db
current_level: db

section "OAM Vars", WRAM0[$C100]
player_sprite: ds 8
weapon_sprite: ds 8
weapon_sprite_defense: ds 8
enemy_1_sprite: ds 8
enemy_2_sprite: ds 8
enemy_3_sprite: ds 8
enemy_4_sprite: ds 8
enemy_5_sprite: ds 8
