section "Levels", rom0

; Setup the level according to whats in the current_level var
SETUP_LEVEL:
    ld a, [current_level]
    cp 1
    jr z, .level_1_init

    cp 2
    jr z, .level_2_init

    cp 3
    jr z, .level_3_init

    cp 4
    jp z, .level_4_init

    cp 5
    jp z, .level_5_init

    cp 6
    jp z, .level_6_init

    cp 7
    jp z, .level_7_init

    cp 8
    jp z, .level_8_init

    ;cp 9
    ;jp z, .level_9_init

    ret

.level_1_init:
    ld hl, Enemy_1_XPos
    ld a, 150
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_3_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_4_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_5_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ret

.level_2_init:
    ; init enemy 1
    ld hl, Enemy_1_XPos
    ld a, 75
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    ld a, 50
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_3_XPos
    ld a, 200
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_4_XPos
    ld a, 230
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_5_XPos
    ld a, 150
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT
    ret

.level_3_init:
    ld hl, Enemy_1_XPos
    ld a, 150
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_3_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_4_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_5_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ret

.level_4_init:
    ld hl, Enemy_1_XPos
    ld a, 150
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    ld a, 70
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_3_XPos
    ld a, 200
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_4_XPos
    ld a, 90
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_5_XPos
    ld a, 230
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ret

.level_5_init:
    ld hl, Enemy_1_XPos
    ld a, 150
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    ld a, 70
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_3_XPos
    ld a, 200
    ld b, a
    ld c, ENEMY_TYPE_1
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_4_XPos
    ld a, 90
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_5_XPos
    ld a, 230
    ld b, a
    ld c, ENEMY_TYPE_2
    call SINGLE_ENEMY_INIT

    ret

.level_6_init:
    ld hl, Enemy_1_XPos
    ld a, 150
    ld b, a
    ld c, ENEMY_TYPE_3
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_3_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_4_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_5_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ret

.level_7_init:
    ld hl, Enemy_1_XPos
    ld a, 150
    ld b, a
    ld c, ENEMY_TYPE_3
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    ld a, 70
    ld b, a
    ld c, ENEMY_TYPE_3
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_3_XPos
    ld a, 200
    ld b, a
    ld c, ENEMY_TYPE_3
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_4_XPos
    ld a, 90
    ld b, a
    ld c, ENEMY_TYPE_3
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_5_XPos
    ld a, 230
    ld b, a
    ld c, ENEMY_TYPE_3
    call SINGLE_ENEMY_INIT

    ret

.level_8_init:
    ld hl, Enemy_1_XPos
    ld a, 250
    ld b, a
    ld c, ENEMY_TYPE_4
    call SINGLE_ENEMY_INIT

    ld hl, Enemy_2_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_3_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_4_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ld hl, Enemy_5_XPos
    call SINGLE_ENEMY_INIT_HIDDEN

    ret


CHECK_IF_LEVEL_COMPLETED:
    ld a, [Enemy_1_Attributes]
    and _ENEMY_IS_DEAD
    jr z, .not_completed

    ld a, [Enemy_2_Attributes]
    and _ENEMY_IS_DEAD
    jr z, .not_completed

    ld a, [Enemy_3_Attributes]
    and _ENEMY_IS_DEAD
    jr z, .not_completed

    ld a, [Enemy_4_Attributes]
    and _ENEMY_IS_DEAD
    jr z, .not_completed

    ld a, [Enemy_5_Attributes]
    and _ENEMY_IS_DEAD
    jr z, .not_completed

    jr .level_completed

.not_completed
    ret

.level_completed
    ; raise the current level
    ld hl, current_level
    inc [hl]

    call SETUP_LEVEL
    ret
