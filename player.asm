Section "Player", rom0

PLAYER_DOWN_LEFT_TILE equ 0
PLAYER_UP_LEFT_TILE equ 8
PLAYER_LEFT_LEFT_TILE equ 18
PLAYER_RIGHT_LEFT_TILE equ 16
SPIRTE_HEIGHT equ 16
SPRITE_WIDTH equ 8
MOVE_SPEED equ 1                ; adjust for TH

; int player vars
PLAYER_INIT:
    ld a, 100                   ; initial start position
    ld [Player_YPos], a
    ld [Player_XPos], a

    ld a, %00000100
    ld [Player_Direction], a
    ld a, PLAYER_DOWN_LEFT_TILE                   ; initial tile
    ld [Player_LeftTileNumber], a

    ;ld a, _DEFENSE_ON           ; DEFENSE MODE
    ;ld a, _TH_ON                 ; TH MODE
    ;ld a, _TH_ON|_DEFENSE_ON    ; ELSIE MODE
    ld a, 0                      ; demo mode

    ld [Player_GameFlags], a

    ld a, 2
    ld [Player_RightTileNumber], a
    ld [Player_Flags], a      ; initial flags (none)
    ld a, 20
    ld [Player_WalkingTimer], a
    ret


; Update the player's information
UPDATE_PLAYER:
    call MOVE_PLAYER
    call UPDATE_ATTACK
    ret

; Update the player on if they are atacking or not
UPDATE_ATTACK:
    ld a, [pad_state]
    and $F0
    swap a              ; swap nibbles to get button state instead of direction

    and P1F_0           ; a button
    call nz, PLAYER_ATTACK
    ;call z, STOP_ATTACK

    ret

; Update the player information to stop the attack animation/info
STOP_ATTACK:
    ; get the attack flag, and ~/not it
    ld a, _PLAYER_ATTACKING
    cpl
    ld b, a
    ; get the current game flags
    ld a, [Player_GameFlags]

    ; or with the inverted mask in b to reset attacking pos
    and b

    ; load back into flags
    ld [Player_GameFlags], a
    ret

; Update the plyaer information to trigger the attack animation/info
PLAYER_ATTACK:
    ld a, [Player_GameFlags]
    or _PLAYER_ATTACKING
    ld [Player_GameFlags], a
    call START_ATTACK
    ret

; Move the player depending on keys pressed
MOVE_PLAYER:
    ld a, [pad_state]
    and $0F
    jr z, .noMovement

    and P1F_3           ; down key
    call nz, MOVE_DOWN

    ld a, [pad_state]
    and P1F_2           ; up key
    call nz, MOVE_UP

    ld a, [pad_state]
    and P1F_1           ; left key
    call nz, MOVE_LEFT

    ld a, [pad_state]
    and P1F_0           ; right key
    call nz, MOVE_RIGHT
    ret

.noMovement
    ; turn off walking animation
    ld a, [Player_GameFlags]
    res 0, a
    ld [Player_GameFlags], a
    ret

; Move the Player down
MOVE_DOWN:
    ld a, _PLAYER_DOWN
    ld [Player_Direction], a

    ; set the correct sprite
    ld a, PLAYER_DOWN_LEFT_TILE
    ld [Player_LeftTileNumber], a
    add 2
    ld [Player_RightTileNumber], a
    ld a, 0
    ld [Player_Flags], a

    ld a, [Player_YPos]
    cp 144-16                 ; check if at the bottom of the screen
    jr z, .scrollDown
    add MOVE_SPEED
    ld [Player_YPos], a

    ld a, [Player_GameFlags]
    set 0, a
    ld [Player_GameFlags], a     ; set walking to true
    ret

.scrollDown:
    ; get the scroll register
    ld a, [rSCY]
    ; check if at bottom of the screen
    cp 112          ; got this number for testing, not entirely sure why it works
    ret z
    add MOVE_SPEED
    ld [rSCY], a
    ret

; Move the Player up
MOVE_UP:
    ld a, _PLAYER_UP
    ld [Player_Direction], a

    ; set the correct sprite
    ld a, PLAYER_UP_LEFT_TILE
    ld [Player_LeftTileNumber], a
    add 2
    ld [Player_RightTileNumber], a
    ld a, 0
    ld [Player_Flags], a

    ld a, [Player_YPos]
    cp 32                   ; check if at the top of the screen
    jr z, .scrollUp
    sub MOVE_SPEED
    ld [Player_YPos], a

    ld a, [Player_GameFlags]
    set 0, a
    ld [Player_GameFlags], a     ; set walking to true
    ret

.scrollUp:
    ; get the scroll register
    ld a, [rSCY]
    ; check if at top of the screen
    cp 0          
    ret z
    sub MOVE_SPEED
    ld [rSCY], a
    ret

; Move the Player right
MOVE_RIGHT:
    ld a, _PLAYER_RIGHT
    ld [Player_Direction], a

    ; set the correct sprite
    ld a, PLAYER_RIGHT_LEFT_TILE
    ld [Player_LeftTileNumber], a
    add 2
    ld [Player_RightTileNumber], a
    ld a, 0
    ld [Player_Flags], a

    ld a, [Player_XPos]
    cp 138                 ; check if at the right most of the screen
    jr z, .scrollRight
    add MOVE_SPEED
    ld [Player_XPos], a

    ld a, [Player_GameFlags]
    set 0, a
    ld [Player_GameFlags], a     ; set walking to true
    ret

.scrollRight:
    ; get the scroll register
    ld a, [rSCX]
    ; check if at bottom of the screen
    cp  96
    ret z
    add MOVE_SPEED
    ld [rSCX], a
    ret

; Move the Player left
MOVE_LEFT:
    ld a, _PLAYER_LEFT
    ld [Player_Direction], a           ; left direction
    ld a, PLAYER_LEFT_LEFT_TILE

    ; set the correct sprite
    ld [Player_LeftTileNumber], a
    sub 2
    ld [Player_RightTileNumber], a
    ld a, OAMF_XFLIP
    ld [Player_Flags], a

    ld a, [Player_XPos]
    cp 23                  ; check if ata the left most of the screen
    jr c, .scrollLeft
    sub MOVE_SPEED
    ld [Player_XPos], a

    ld a, [Player_GameFlags]
    set 0, a
    ld [Player_GameFlags], a     ; set walking to true
    ret

.scrollLeft:
    ; get the scroll register
    ld a, [rSCX]
    ; check if at left of the screen
    cp 0
    ret z
    sub MOVE_SPEED
    ld [rSCX], a
    ret

; Update the walking var/timer
UPDATE_WALKING:
    ld a, [Player_GameFlags]             ; is the player moving?
    and _PLAYER_IS_WALKING
    ret z                              ; if no return

    ld a, [Player_WalkingTimer]        ; check if we need to update the sprite
    dec a
    ld [Player_WalkingTimer], a
    cp 0
    ret nz                               ; return if no need to update

.changeWalkingSprite
    ld a, 5                             ; tweak this to make the sprite change faster for walking
    ld [Player_WalkingTimer], a

    ld a, [Player_GameFlags]             ; are we currently changing the sprite due to walking?
    and _PLAYER_USE_ALT_SPRITE
    jr nz, .revertWalking               ; if yes, revet

    ; otherwise turn on
    ld a, [Player_GameFlags]
    or _PLAYER_USE_ALT_SPRITE
    ld [Player_GameFlags], a
    ret                                 ; updated, now return

.revertWalking
    ; otherwise turn on
    ld a, [Player_GameFlags]
    xor a, _PLAYER_USE_ALT_SPRITE
    ld [Player_GameFlags], a
    ret

; Check to see if the alt walking sprite should be used
CHECK_ALT_SPRITE:
    ld a, [Player_GameFlags]             ; check if the alt sprite needs to be used
    and _PLAYER_USE_ALT_SPRITE
    ret z                               ; if not, should already be ok, return

    ld a, [Player_GameFlags]             ; now check if we're moving
    and _PLAYER_IS_WALKING
    ret z                               ; if not, we've probably already updated the sprite, don't need to do again

    ld a, [Player_LeftTileNumber]          ; get the alt sprite for both left and right
    add 4
    ld [Player_LeftTileNumber], a

    ld a, [Player_RightTileNumber] 
    add 4
    ld [Player_RightTileNumber], a
    ret

; Draw the player sprites to shadow OAMRAM
DRAW_PLAYER:
    call UPDATE_WALKING
    call CHECK_ALT_SPRITE

.draw
    ld hl, player_sprite

    ld a, [Player_YPos]
    ld [hli], a
    ld a, [Player_XPos]
    ld [hli], a
    ld a, [Player_LeftTileNumber]
    ld [hli], a
    ld a, [Player_Flags]
    ld [hli], a

    ld a, [Player_YPos]
    ld [hli], a
    ld a, [Player_XPos]
    add 8
    ld [hli], a
    ld a, [Player_RightTileNumber]
    ld [hli], a
    ld a, [Player_Flags]
    ld [hli], a

    ret


; Checks if the player is hit by a specific enemy
; params:
;   - b: [Player_XPos]
;   - c: [Player_XPos] + SPRITE_WIDTH
;   - d: [Player_YPos]
;   - e: [Player_YPos] + SPRITE_HEIGHT
;   - hl: Enemy_Attributes
; returns:
;   - a: 1 if hit, 0 otherwise
; destroys:
;   - hl
CHECK_IF_PLAYER_HIT_BY_SPECIFIC_ENEMY:
    ; see if the enemy is dead first
    ; increase hl here since its less cycles than an inc
    ld a, [hl+]
    and _ENEMY_IS_DEAD|_ENEMY_NOT_ON_SCREEN
    jr nz, .is_not_hit

    ; hl = Enemy_ScreenXPos
    inc hl

    ; enemy.x > player.x + width
    ; inc hl => Enemy_ScreenYPos
    ld a, [hl+]
    cp c
    jr nc, .is_not_hit

    ; enemy.x+width < player.x
    add SPRITE_WIDTH
    cp b
    jr c, .is_not_hit

    ; enemy.y > player.y + height
    ld a, [hl]
    cp e
    jr nc, .is_not_hit
    
    ; enemy.y+height < player.y
    add SPIRTE_HEIGHT
    cp d
    jr c, .is_not_hit

    ld a, 1
    ret

.is_not_hit:
    ld a, 0
    ret

; Checks if the player is hit by any enemy
; Returns:
;   - a: set to 1 if hit, (0 otherwise)
; Destroys:
;   - b
;   - c
;   - d
;   - e
;   - hl
CHECK_IF_PLAYER_HIT:
    ; set b = player_x
    ld a, [Player_XPos]
    ld b, a
    ; c = player_x + width
    add SPRITE_WIDTH
    ld c, a
    ; d = player_y
    ld a, [Player_YPos]
    ld d, a
    ; e = player_y + height
    add SPIRTE_HEIGHT
    ld e, a


    ld hl, Enemy_1_Attributes
    call CHECK_IF_PLAYER_HIT_BY_SPECIFIC_ENEMY

    cp 1
    ld hl, Enemy_2_Attributes
    call nz, CHECK_IF_PLAYER_HIT_BY_SPECIFIC_ENEMY

    cp 1
    ld hl, Enemy_3_Attributes
    call nz, CHECK_IF_PLAYER_HIT_BY_SPECIFIC_ENEMY

    cp 1
    ld hl, Enemy_4_Attributes
    call nz, CHECK_IF_PLAYER_HIT_BY_SPECIFIC_ENEMY

    cp 1
    ld hl, Enemy_5_Attributes
    call nz, CHECK_IF_PLAYER_HIT_BY_SPECIFIC_ENEMY

    ret


Section "Player Struct", wram0
    dstruct Character, Player     ; create Player info