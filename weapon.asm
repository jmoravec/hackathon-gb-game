Section "Weapon", rom0

WEAPON_DOWN_XOFFSET equ 0
WEAPON_DOWN_YOFFSET equ 15
WEAPON_UP_XOFFSET equ 0
WEAPON_UP_YOFFSET equ 15
WEAPON_RIGHT_XOFFSET equ 12
WEAPON_RIGHT_YOFFSET equ 5
WEAPON_LEFT_XOFFSET equ 12
WEAPON_LEFT_YOFFSET equ 5

WEAPON_DOWN_TILE equ $18
WEAPON_UP_TILE equ $1C
WEAPON_RIGHT_TILE equ $20
WEAPON_LEFT_TILE equ $22

WEAPON_INIT:
    ; init animate timer
    ld hl, Sword_AnimateTimer
    ld [hl], 0
    call UPDATE_WEAPON
    ret

START_ATTACK:
    ld hl, Sword_AnimateTimer
    ld [hl], 20
    ret

SET_WEAPON_DEFENSE_TILE:
    ld a, [Player_Direction]
    and _PLAYER_DOWN
    jr nz, .up

    ld a, [Player_Direction]
    and _PLAYER_UP
    jr nz, .down

    ld a, [Player_Direction]
    and _PLAYER_LEFT
    jr nz, .right

    ld a, [Player_Direction]
    and _PLAYER_RIGHT
    jr nz, .left

    ; shouldn't hit, but just in case Player_direction isn't set
    ret                         

.down:
    ld a, WEAPON_DOWN_TILE
    ld [Sword_Defense_LeftTileNumber], a
    add 2
    ld [Sword_Defense_RightTileNumber], a
    ld a, 0
    ld [Sword_Defense_Flags], a
    ret

.up:
    ld a, WEAPON_UP_TILE
    ld [Sword_Defense_LeftTileNumber], a
    add 2
    ld [Sword_Defense_RightTileNumber], a
    ld a, 0
    ld [Sword_Defense_Flags], a
    ret

.left:
    ld a, WEAPON_LEFT_TILE
    ld [Sword_Defense_LeftTileNumber], a
    sub 2
    ld [Sword_Defense_RightTileNumber], a
    ld a, OAMF_XFLIP
    ld [Sword_Defense_Flags], a
    ret

.right:
    ld a, WEAPON_RIGHT_TILE
    ld [Sword_Defense_LeftTileNumber], a
    add 2
    ld [Sword_Defense_RightTileNumber], a
    ld a, 0
    ld [Sword_Defense_Flags], a
    ret

SET_WEAPON_TILE:
    ld a, [Player_Direction]
    and _PLAYER_DOWN
    jr nz, .down

    ld a, [Player_Direction]
    and _PLAYER_UP
    jr nz, .up

    ld a, [Player_Direction]
    and _PLAYER_LEFT
    jr nz, .left

    ld a, [Player_Direction]
    and _PLAYER_RIGHT
    jr nz, .right

    ; shouldn't hit, but just in case Player_direction isn't set
    ret                         

.down:
    ld a, WEAPON_DOWN_TILE
    ld [Sword_LeftTileNumber], a
    add 2
    ld [Sword_RightTileNumber], a
    ld a, 0
    ld [Sword_Flags], a
    ret

.up:
    ld a, WEAPON_UP_TILE
    ld [Sword_LeftTileNumber], a
    add 2
    ld [Sword_RightTileNumber], a
    ld a, 0
    ld [Sword_Flags], a
    ret

.left:
    ld a, WEAPON_LEFT_TILE
    ld [Sword_LeftTileNumber], a
    sub 2
    ld [Sword_RightTileNumber], a
    ld a, OAMF_XFLIP
    ld [Sword_Flags], a
    ret

.right:
    ld a, WEAPON_RIGHT_TILE
    ld [Sword_LeftTileNumber], a
    add 2
    ld [Sword_RightTileNumber], a
    ld a, 0
    ld [Sword_Flags], a
    ret

SET_WEAPON_DEFENSE_POSITION:
    ; check if in an attack animation
    ld hl, Sword_AnimateTimer
    ld a, [hl]
    cp 0
    jr z, .notAttacking

    ; save timer to b and decrese it
    ld b, a
    dec [hl]

    ld a, [Player_Direction]
    and _PLAYER_DOWN
    jr nz, .up

    ld a, [Player_Direction]
    and _PLAYER_UP
    jr nz, .down

    ld a, [Player_Direction]
    and _PLAYER_LEFT
    jr nz, .right

    ld a, [Player_Direction]
    and _PLAYER_RIGHT
    jr nz, .left
    ; shouldn't hit, but just in case Player_direction isn't set
    ret                         

.notAttacking:
    ; set y position off screen
    ld a, 160
    ld [Sword_Defense_YPos], a
    ret

.down:
    ld a, [Player_YPos]
    add WEAPON_DOWN_YOFFSET
    ld [Sword_Defense_YPos], a

    ld a, [Player_XPos]
    ;sub WEAPON_DOWN_XOFFSET
    add b
    ld [Sword_Defense_XPos], a
    ret
.up:
    ld a, [Player_YPos]
    sub WEAPON_UP_YOFFSET
    ld [Sword_Defense_YPos], a

    ld a, [Player_XPos]
    ;sub WEAPON_UP_XOFFSET
    add b
    ld [Sword_Defense_XPos], a
    ret
.left:
    ld a, [Player_YPos]
    ;sub WEAPON_LEFT_YOFFSET
    add b
    ld [Sword_Defense_YPos], a

    ld a, [Player_XPos]
    sub WEAPON_LEFT_XOFFSET
    ld [Sword_Defense_XPos], a
    ret
.right:
    ld a, [Player_YPos]
    ;sub WEAPON_RIGHT_YOFFSET
    add b
    ld [Sword_Defense_YPos], a

    ld a, [Player_XPos]
    add WEAPON_RIGHT_XOFFSET
    ld [Sword_Defense_XPos], a
    ret

SET_WEAPON_POSITION:
    ; check if in an attack animation
    ld hl, Sword_AnimateTimer
    ld a, [hl]
    cp 0
    jr z, .notAttacking

    ; save timer to b and decrese it
    ld b, a
    dec [hl]

    ld a, [Player_Direction]
    and _PLAYER_DOWN
    jr nz, .down

    ld a, [Player_Direction]
    and _PLAYER_UP
    jr nz, .up

    ld a, [Player_Direction]
    and _PLAYER_LEFT
    jr nz, .left

    ld a, [Player_Direction]
    and _PLAYER_RIGHT
    jr nz, .right
    ; shouldn't hit, but just in case Player_direction isn't set
    ret                         

.notAttacking:
    ; set y position off screen
    ld a, 160
    ld [Sword_YPos], a
    ret

.down:
    ld a, [Player_YPos]
    add WEAPON_DOWN_YOFFSET
    ld [Sword_YPos], a

    ld a, [Player_XPos]
    ;sub WEAPON_DOWN_XOFFSET
    add b
    ld [Sword_XPos], a
    ret
.up:
    ld a, [Player_YPos]
    sub WEAPON_UP_YOFFSET
    ld [Sword_YPos], a

    ld a, [Player_XPos]
    ;sub WEAPON_UP_XOFFSET
    add b
    ld [Sword_XPos], a
    ret
.left:
    ld a, [Player_YPos]
    ;sub WEAPON_LEFT_YOFFSET
    add b
    ld [Sword_YPos], a

    ld a, [Player_XPos]
    sub WEAPON_LEFT_XOFFSET
    ld [Sword_XPos], a
    ret
.right:
    ld a, [Player_YPos]
    ;sub WEAPON_RIGHT_YOFFSET
    add b
    ld [Sword_YPos], a

    ld a, [Player_XPos]
    add WEAPON_RIGHT_XOFFSET
    ld [Sword_XPos], a
    ret


UPDATE_WEAPON:
    call SET_WEAPON_TILE
    call SET_WEAPON_POSITION

    ld a, [Player_GameFlags]
    and _DEFENSE_ON
    call nz, SET_WEAPON_DEFENSE_TILE

    ld a, [Player_GameFlags]
    and _DEFENSE_ON
    call nz, SET_WEAPON_DEFENSE_POSITION
    ret

; Draw the player sprites to shadow OAMRAM
DRAW_WEAPON:
.draw
    ld hl, weapon_sprite

    ld a, [Sword_YPos]
    ld [hli], a
    ld a, [Sword_XPos]
    ld [hli], a
    ld a, [Sword_LeftTileNumber]
    ld [hli], a
    ld a, [Sword_Flags]
    ld [hli], a

    ld a, [Sword_YPos]
    ld [hli], a
    ld a, [Sword_XPos]
    add 8
    ld [hli], a
    ld a, [Sword_RightTileNumber]
    ld [hli], a
    ld a, [Sword_Flags]
    ld [hli], a

    ld a, [Player_GameFlags]
    and _DEFENSE_ON
    jr z, .no_defense

    ld hl, weapon_sprite_defense

    ld a, [Sword_Defense_YPos]
    ld [hli], a
    ld a, [Sword_Defense_XPos]
    ld [hli], a
    ld a, [Sword_Defense_LeftTileNumber]
    ld [hli], a
    ld a, [Sword_Defense_Flags]
    ld [hli], a

    ld a, [Sword_Defense_YPos]
    ld [hli], a
    ld a, [Sword_Defense_XPos]
    add 8
    ld [hli], a
    ld a, [Sword_Defense_RightTileNumber]
    ld [hli], a
    ld a, [Sword_Defense_Flags]
    ld [hli], a
    ret

.no_defense
    ld hl, weapon_sprite_defense

    ld a, 160
    ld [hli], a
    ld a, [Sword_Defense_XPos]
    ld [hli], a
    ld a, [Sword_Defense_LeftTileNumber]
    ld [hli], a
    ld a, [Sword_Defense_Flags]
    ld [hli], a

    ld a, 160
    ld [hli], a
    ld a, [Sword_Defense_XPos]
    add 8
    ld [hli], a
    ld a, [Sword_Defense_RightTileNumber]
    ld [hli], a
    ld a, [Sword_Defense_Flags]
    ld [hli], a
    ret


Section "Weapon Struct", wram0
    dstruct Weapon, Sword
    dstruct Weapon, Sword_Defense  