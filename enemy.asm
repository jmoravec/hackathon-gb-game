ENEMY_TYPE_1 equ 1
ENEMY_TYPE_2 equ 2
ENEMY_TYPE_3 equ 3
ENEMY_TYPE_4 equ 4
ENEMY_TYPE_1_TILE equ $24
ENEMY_TYPE_2_TILE equ $28
ENEMY_TYPE_3_TILE equ $2C
ENEMY_TYPE_4_TILE equ $30

WALL_BARRIOR equ 20

section "Enemy Logic", rom0

; Set the enemy type to the given one
; Params:
;   - hl, pointer to enemy LeftTileNumber
;   - c,  type the enemy should be
; Return:
;   - hl, pointer to Enemy_flags
; Destroys:
;   - a
;   - hl
SET_ENEMY_TYPE:
    ld a, c
    cp ENEMY_TYPE_1
    jr z, .type_1

    cp ENEMY_TYPE_2
    jr z, .type_2

    cp ENEMY_TYPE_3
    jr z, .type_3

    cp ENEMY_TYPE_4
    jr z, .type_4

    ret

.type_1
    ld a, ENEMY_TYPE_1_TILE
    ld [hl+], a
    add $2
    ld [hl+], a
    ld a, _ENEMY_HORIZONTAL_ONLY
    ld [hl+], a
    ret

.type_2
    ld a, ENEMY_TYPE_2_TILE
    ld [hl+], a
    add $2
    ld [hl+], a
    ld a, _ENEMY_VERTICAL_ONLY
    ld [hl+], a
    ret

.type_3
    ld a, ENEMY_TYPE_3_TILE
    ld [hl+], a
    add $2
    ld [hl+], a
    ld a, _ENEMY_VERTICAL_ONLY|_ENEMY_HORIZONTAL_ONLY
    ld [hl+], a
    ret

.type_4
    ld a, ENEMY_TYPE_4_TILE
    ld [hl+], a
    add $2
    ld [hl+], a
    ld a, _ENEMY_PLAYER_FOLLOWER
    ld [hl+], a
    ret
    
; Init a single enemy
; Params:
;   - hl: struct xpos pointer
;   - a: initial x pos
;   - b: initial y pos
;   - c: type
; Returns:
;   - hl: Enemy_Flags
; Destroys:
;   - a
SINGLE_ENEMY_INIT:
    ld [hl+], a         ; xpos
    ld a, b
    ld [hl+], a         ; ypos
    call SET_ENEMY_TYPE 
    ld [hl], 0          ; flags
    ret

; Init a single enemy to be "dead" at init
; Params:
;   - hl: Enemy_XPos
; Returns:
;   - hl: Enemy_Flags
; Destroys:
;   - a
SINGLE_ENEMY_INIT_HIDDEN:
    inc hl
    ld a, 160
    ld [hl+], a
    inc hl
    inc hl          ; Enemy_Attributes
    ld a, _ENEMY_IS_DEAD
    ld [hl+], a
    ld [hl], 0      ; Flags
    ret

; Move the enemy according to its attributes
; Params:
;   - hl pointer to enemy_attributes
; Destroys:
;   - de
;   - a
;   - b
;   - c
MOVE_ENEMY:
    push hl             ; save for later

    ld d, h
    ld e, l
    inc de
    inc de
    inc de
    inc de              ; point de to direction

    ld a, [hl-]
    ld c, a
    dec hl
    dec hl
    dec hl              ; get hl to point to enemy_xpos

    and a, _ENEMY_PLAYER_FOLLOWER
    jr nz, .towards_player

    ; check if horizontal enemy
    ld a, c
    and a, _ENEMY_HORIZONTAL_ONLY
    call nz, MOVE_ENEMY_HORIZONTAL

    ; check if vertical enemy
    inc hl      ; hl = Enemy_YPos
    ld a, c
    and _ENEMY_VERTICAL_ONLY
    call nz, MOVE_ENEMY_VERTICAL

    jr .done

.towards_player:
    call MOVE_ENEMY_TOWARDS_PLAYER
    jr .done

.done:
    pop hl
    ret

; Move the enemy towards the player character
; Params:
;   - hl: Enemy_XPos
; Destroys:
;   - hl
;   - a
;   - b
;   - c
MOVE_ENEMY_TOWARDS_PLAYER:
    ; b = [Player_XPos]
    ld a, [Player_XPos]
    ld b, a
    ; a = [rSCX]
    ld a, [rSCX]
    ; a = [Player_XPos] + [rSCX]
    ; aka Player Map X Pos
    add b
    ; save to b
    ld b, a

    ; do the same thing for player.y and save to c
    ld a, [Player_YPos]
    ld c, a
    ld a, [rSCY]
    add c
    ld c, a

.check_x_pos
    ; a = [Enemy_XPos]
    ld a, [hl]
    ; compare with player's x pos
    cp b
    ; if enemy.x < player.x increase enemy.x
    jr c, .increase_x
    ; otherwise decrease enemy.x
    dec [hl]
    jr .check_y_pos

.increase_x
    inc [hl]
    jr .check_y_pos

.check_y_pos
    ; get hl to point to enemy.y
    inc hl
    ld a, [hl]
    ; compare with player's y pos
    cp c
    ; if enemy.y < player.y increase enemy.y
    jr c, .increase_y
    ; otherwise decrease enemy.y
    dec [hl]
    ; can return now
    ret

.increase_y
    inc [hl]
    ret



; Move the enemy up or down
; Params:
;   - hl: Enemy_YPos
;   - de: enemy_CurrentDirection pointer
; Destroys:
;   - a
;   - b (only in certain cases)
;   - c
MOVE_ENEMY_VERTICAL:
    ; get direction
    ld a, [de]
    and _ENEMY_MOVING_DOWN
    jr nz, .move_down

    ; move down
    ld a, [hl]
    dec a
    cp WALL_BARRIOR     ; check if hit up wall
    jr c, .start_moving_down
    ld [hl], a
    ret

.start_moving_down
    ld a, [de]
    or _ENEMY_MOVING_DOWN
    ld [de], a
    ret

.move_down
    ld b, [hl]
    inc b
    ld a, 255-WALL_BARRIOR
    cp b
    jr c, .start_moving_up
    ld [hl], b
    ret

.start_moving_up
    ; inverse mask
    ld a, _ENEMY_MOVING_DOWN
    cpl
    ld b, a

    ld a, [de]
    and b
    ld [de], a
    ret

; Move the enemy left or right
; Params:
;   - hl enemy_XPos
;   - de: enemy_direction pointer
; Destroys:
;   - a
;   - b (only in certain cases)
MOVE_ENEMY_HORIZONTAL:
    ; get direction
    ld a, [de]
    and _ENEMY_MOVING_RIGHT
    jr nz, .move_right

    ; move left
    ld a, [hl]      ; set a = enemy_xpos
    dec a
    cp WALL_BARRIOR            ; check if hit left wall
    jr c, .start_moving_right
    ; else move the enemy
    ld [hl], a
    ret

.start_moving_right:
    ld a, [de]
    or _ENEMY_MOVING_RIGHT
    ld [de], a
    ret

.move_right:
    ld b, [hl]      ; set c = enemy_xpos
    inc b
    ld a, 255-WALL_BARRIOR
    cp b            ; check if hit right wall
    jr c, .start_moving_left
    ; else move the enemy
    ld [hl], b
    ret

.start_moving_left:
    ; inverse mask
    ld a, _ENEMY_MOVING_RIGHT
    cpl
    ld b, a

    ld a, [de]
    and b
    ld [de], a
    ret


; Update a specific enemy
; params:
;   - hl: pointer to Enemy_Attributes
UPDATE_ENEMY:
    ld a, [hl]
    and _ENEMY_IS_DEAD|_ENEMY_NOT_ON_SCREEN
    jr nz, .is_dead

    ; move enemy
    call MOVE_ENEMY

    ; check if the player is currently attacking
    ld a, [Player_GameFlags]
    and _PLAYER_ATTACKING
    ; if so, check to see if the enemy is hit
    call nz, CHECK_IF_ENEMY_HIT

    ret

.is_dead:
    dec hl
    dec hl
    dec hl          ; point to Enemy_YPos
    ld [hl], 160
    ret

; Updates All the enemies
UPDATE_ENEMIES:
    ; check if the enemy is off screen/dead
    ld hl, Enemy_1_Attributes
    call UPDATE_ENEMY

    ld hl, Enemy_2_Attributes
    call UPDATE_ENEMY

    ld hl, Enemy_3_Attributes
    call UPDATE_ENEMY

    ld hl, Enemy_4_Attributes
    call UPDATE_ENEMY

    ld hl, Enemy_5_Attributes
    call UPDATE_ENEMY

    ret


; Checks if the enemy is hit by the player attack
; sets enemy to be dead if hit
; params:
;   - hl pointer to Enemy_Attributes
CHECK_IF_ENEMY_HIT:
    ; save for enemy_attributes later
    push hl
    ; inc hl twice to get to enemy_ScreenXPos
    inc hl
    inc hl

    ; set b = weapon.x
    ld a, [Sword_XPos]
    ld b, a
    ; c = player_x + width
    add SPRITE_WIDTH
    ld c, a
    ; d = player_y
    ld a, [Sword_YPos]
    ld d, a
    ; e = player_y + height
    add SPIRTE_HEIGHT
    ld e, a

    ; check Enemy 1
    ; enemy.x > player.x + width
    ld a, [hli]
    cp c
    jr nc, .is_not_hit
    ; enemy.x+width < player.x
    add SPRITE_WIDTH
    cp b
    jr c, .is_not_hit

    ; enemy.y > player.y + height
    ld a, [hl]
    cp e
    jr nc, .is_not_hit
    ; enemy.y+height < player.y
    add SPIRTE_HEIGHT
    cp d
    jr c, .is_not_hit

    ; set is dead attribute
    pop hl
    ld a, [hl]
    or _ENEMY_IS_DEAD
    ld [hl], a
    ret

.is_not_hit:
    pop hl          ; do this because otherwise the return is screwed up huuuugly 
                    ; i believe its because the ret command expects the pc to be on the top of the stack
                    ; and since hl is, we jump/execute to somewhere we didn't expect to go
    ret

; Update an enemy's position relative to the screen
; params:
;   - hl: pointer to Enemy_Attributes (won't affect)
UPDATE_ENEMY_RELATIVE_POSITION:
    push hl                 ; save for later
    push de

    ; get hl to point to YPos  
    ld de, -3
    add hl, de

    ; b = screen.y
    ld a, [rSCY]
    ld b, a
    ; a = [Enemy_YPos]
    ld a, [hl]

    ; relative enemy y pos = enemy.y - screen.y
    sub b

    ; set c to y pos of first sprite
    ld c, a
    
    ; get hl to point to ScreenYPos
    ld de, 6
    add hl, de

    ; set screen y pos of enemy
    ld [hl], c              

    ; get hl to  point to xpos
    ld de, -7
    add hl, de

    ; b = screen.x
    ld a, [rSCX]
    ld b, a
    ; a = [Enemy_XPos]
    ld a, [hl]

    ; get hl to point to ScreenXPos
    ld de, 6
    add hl, de

    ; relative enemy x pos = enemy.x - screen.x
    sub b
    ; set d to x pos of first sprite
    ld d, a
    ; set screen x pos of enemy
    ld [hl], d

    pop de
    pop hl                  ; get old values back
    ret

; Draw a specific Enemy, uses all registers
; params:
;   - hl: pointer to Enemy_Attributes
;   - de: pointer to enemy_sprite in shadow ORAM
DRAW_ENEMY:
    ld a, [hl]
    and _ENEMY_NOT_ON_SCREEN|_ENEMY_IS_DEAD
    jr nz, .dont_draw

    call UPDATE_ENEMY_RELATIVE_POSITION

    ; get hl to point to ScreenYPos
    ld bc, 3
    add hl, bc

    ; a = [Enemy_ScreenYPos]
    ; shadow ORAM YPos = a
    ; hl = Enemy_ScreenXPos
    ; de = SORAM XPos
    ld a, [hl-]
    ld [de], a                 ; load y into shadow oam
    inc de

    ; a = [Enemy_ScreenXPos]
    ; SORAM_YPos = a
    ; hl = Enemy_LeftTileNumber
    ; de = SORAM_TileNum
    ld a, [hl]
    ld [de], a                 ; load x into shadow oam
    ld bc, -4
    add hl, bc
    inc de

    ; a = [Enemy_LeftTileNumber]
    ; SORAM_TileNum = a
    ; hl = Enemy_Flags
    ; de = SORAM_Flags
    ld a, [hl]
    ld [de], a
    ld bc, 3
    add hl, bc
    inc de

    ; a = [Enemy_Flags]
    ; SORAM_Flags = a
    ; hl = ScreenYPos
    ; de = EnemyRight_YPos
    ld a, [hl+]
    ld [de], a
    inc hl
    inc de

    ; a = [Enemy_ScreenYPos]
    ; EnemyRIGHT_YPos = a
    ; hl = Enemy_ScreenXPos
    ; de = EnemyRight_XPos
    ld a, [hl-]
    ld [de], a
    inc de

    ; a = [Enemy_ScreenXPos] + 8
    ; EnemyRight_XPos = a
    ; hl = Enemy_RightTileNumber
    ; de = EnemyRight_Tile
    ld a, [hl]
    add 8           ; add 8 for right side sprite offset (width of sprite)
    ld [de], a
    ld bc, -3
    add hl, bc
    inc de

    ; a = [Enemy_RightTileNumber]
    ; EnemyRight_Tile = a
    ; hl = Enemy_Flags
    ; de = EnemyRight_Flags
    ld a, [hl+]
    ld [de], a
    inc hl
    inc de

    ; a = [Enemy_Flags]
    ; EnemyRight_Flags = a
    ld a, [hl]
    ld [de], a
    ret

.dont_draw:
    ld a, 160               ; draw offscreen
    ld [de], a              ; load x pos to shadow oram
    inc de
    inc de
    inc de
    inc de                  ; inc de to point to 2nd half of sprite
    ld a, 160               ; draw 2nd half off screen
    ld [de], a
    ret

DRAW_ENEMIES:

    ld hl, Enemy_1_Attributes
    ld de, enemy_1_sprite
    call DRAW_ENEMY

    ld hl, Enemy_2_Attributes
    ld de, enemy_2_sprite
    call DRAW_ENEMY

    ld hl, Enemy_3_Attributes
    ld de, enemy_3_sprite
    call DRAW_ENEMY

    ld hl, Enemy_4_Attributes
    ld de, enemy_4_sprite
    call DRAW_ENEMY

    ld hl, Enemy_5_Attributes
    ld de, enemy_5_sprite
    call DRAW_ENEMY

    ret

Section "Enemy Structs", wram0
    dstruct Enemy, Enemy_1
    dstruct Enemy, Enemy_2
    dstruct Enemy, Enemy_3
    dstruct Enemy, Enemy_4
    dstruct Enemy, Enemy_5