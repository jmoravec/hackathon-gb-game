CLEAR_MAP:
    ld  hl,_SCRN0
    ld  bc,$400
    push hl

.clear_map_loop
    ;wait for hblank
    ld  hl, rSTAT
    bit 1, [hl]
    jr  nz, .clear_map_loop
    pop hl

    ld  a, $0
    ld  [hli], a
    push hl

    dec bc
    ld  a,b
    or  c
    jr  nz, .clear_map_loop
    pop hl
    ret

CLEAR_OAM:
    ld  hl, _OAMRAM
    ld  bc, $A0
.clear_oam_loop
    ld  a, $0
    ld  [hli], a
    dec bc
    ld  a, b
    or  c
    jr  nz, .clear_oam_loop
    ret

CLEAR_RAM:
    ld  hl, $C100
    ld  bc, $A0
.clear_ram_loop
    ld  a, $0
    ld  [hli], a
    dec bc
    ld  a, b
    or  c
    jr  nz, .clear_ram_loop
    ret

; Wait for vblank, uses hl
WAIT_VBLANK:
    ld  hl,vblank_flag
.wait_vblank_loop
    halt
    nop        ;Hardware bug
    ld  a,$0
    cp  [hl]
    jr  z, .wait_vblank_loop
    ld  [hl], a
    ret


; DMA copy util, see https://gbdev.gg8.se/wiki/articles/OAM_DMA_tutorial
DMA_COPY:
    ; load de with the HRAM destination address
    ld  de,$FF80
    rst $28

    ; the amount of data we want to copy into HRAM, $000D which is 13 bytes
    DB  $00,$0D

    ; this is the above DMA subroutine hand assembled, which is 13 bytes long
    DB  $F5, $3E, $C1, $EA, $46, $FF, $3E, $28, $3D, $20, $FD, $F1, $D9
    ret

; Read the Joypad state into [pad_state], (1 means button is pressed)
; Destroys:
;   - A 
READ_JOYPAD:
    ld a, P1F_4     ; get button keys
    ld [rP1], a

    ; settle bounce
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]

    
    and $0F                 ; get lower nibble
    swap a                  ; swap nibbles so we can put direction keys on lower nibble
    ld b, a                 ; temp hold into b
    
    ld a, P1F_5     ; get direction keys
    ld [rP1], a

    ; settle bounce
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]

    and $0F                 ; get lower nibble
    or b                    ; or with button key presses

    cpl                     ; 0 means button is pressed, so flip the bits to make it easier to read
    ld [pad_state], a
    ret

; Copy a sprite to a location in memory
; params: hl - location to copy to
;         de - Start location of the sprite to copy
;         bc - Bytes to copy
COPY_SPRITE:
.loop
    ld a, [de]  ; grab 1 byte from source
    ld [hli], a ; Place it into [hl], inc hl
    inc de      ; next byte
    dec bc      ; dec count
    ld a, b     ; check if count is 0, since dec bc doesn't update zero flag
    or c
    jr nz, .loop
    ret

; Copy a sprite to a location in memory
; params: hl - location to copy to
;         de - Start location of the string to copy
;         bc - Bytes to copy
COPY_STRING:
.loop
    ld a, [de]  ; grab 1 byte from source
    add $80
    ld [hli], a ; Place it into [hl], inc hl
    inc de      ; next byte
    dec bc      ; dec count
    ld a, b     ; check if count is 0, since dec bc doesn't update zero flag
    or c
    jr nz, .loop
    ret

; init game palletes
INIT_GAME_PALLETE:
    ;ld a, %01101100
    ld a, %00011011
    ld [rBGP], a
    ld a, %00011110     ; pallete
    ld [rOBP0], a
    ret

; reset scroll
INIT_WINDOW:
    xor a               ; ld a, 0
    ld [rSCY], a
    ld [rSCX], a
    ret


;* Random # - Calculate as you go *
; (Allocate 3 bytes of ram labeled 'Seed')
; Exit: A = 0-255, random number
; from http://www.devrs.com/gb/files/random.txt
RANDOM_NUMBER:
    ld      hl,Seed
    ld      a,[hl+]
    sra     a
    sra     a
    sra     a
    xor     [hl]
    inc     hl
    rra
    rl      [hl]
    dec     hl
    rl      [hl]
    dec     hl
    rl      [hl]
    ld      a,[$fff4]          ; get divider register to increase randomness
    add     [hl]
    ret

section "Seed", wram0
Seed: ds 3